## Kotlin is:

* Used not only on Android
* Easy to learn (IMHO)
* Modern

---

## Kotlin is:

* `fun`

---

### Thank you!

@fa[twitter gp-contact]() @bulaleniwa

@fa[github gp-contact]() lazybun

@fa[gitlab gp-contact]() lazybun

@fa[envelope gp-contact]() krzysztof@piszkod.pl