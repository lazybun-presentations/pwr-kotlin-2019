## Why I like it

+++

@ul

* Tooling
* Expressivnes
* FEATURES

@ulend

+++

## So let's talk features

+++

## Type inference

+++

```java
public static void main(String[] args) {
  User user = new User();

  user.setUsername("JUG");
  user.setPassword("hunter2");

  final String username = user.getUsername();
}
```

Note:
Here we have simple Java code which initializes variable user, sets username and password data to it and then stores username into a finalized variable.

+++

```kotlin
fun main(args: Array<String>) {
  var user = User(
      username = "JUG",
      password = "hunter2"
  )
  
  val username = user.username
}
```

@[2](Regular **var**iable)
@[7](Final **val**ue)

Note:
Here's the same code in Kotlin - difference being the type inference. For your disposal, there are two keywords:
* var - which is a regular variable
* val - which is a finalized variable

+++

## Data classes

+++

```java
public class User {

  private String username;

  private String password;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return Objects.equals(username, user.username) &&
        Objects.equals(password, user.password);
  }

  @Override public int hashCode() {

    return Objects.hash(username, password);
  }

  @Override public String toString() {
    return "User{" +
        "username='" + username + '\'' +
        ", password='" + password + '\'' +
        '}';
  }
}
```

@[3-5](Fields)
@[6-21](Getters & Setters)
@[23-33](Equals)
@[35-38](Hash)
@[40-45](toString)

Note:
As you very well know, creating data class in java is cumbersome. You need to: (list stuff). Sure, your IDE can generate it for you, but still - this is a lot of code.

+++

```kotlin
data class User(
    var username: String,
    var password: String
)
```

@[1-4](Fields, Getters, Setters, Equals, Hash, toString and final)

Note:
In Kotlin, all of this stuff is neatly wrapped in data class. 

+++

## Null safety

Note:
One of killer features

+++

```java
User user = null;

if (user != null) {
  user.doStuff();
}
```

Note:
You're most likely all too familiar with null checking in Java. If you forget it - it could lead to fatal NullPointerException. 

+++

```kotlin
val user: User? = null

user?.doStuff() 
```
@[1](Nullable value)
@[3](Do stuff if user is not null)

Note:
In Kotlin, there null are safe. When declaring a variable, you decide whether it's nullable or not. It's very simple - just add ? to type definition!

+++

## Smart casts

+++

```java
Object o = new User();

if (o instanceof User) {
  User user = (User) o;
  
  user.setUsername("JUG");
  user.setPassword("hunter2");
}
```

@[3](Check type)
@[4](Cast)
@[6-7](Use)

+++

```kotlin
var o: Any = User()

if (o is User) {
  o.username = "JUG"
  o.password = "hunter2"
}
```

@[3](Check type)
@[4-5](Smart cast!)

+++

```kotlin
val user: User? = User()

user?.doSomething()

if (user != null) {
  user.doSomething() //user: User? -> User
}
```

@[3](Nullable :()
@[5](Check whether user is null)
@[6](User smartcast to non nullable!)

+++

## Default arguments

+++

### Constructor

```kotlin
data class User(
    var username: String,
    var password: String = ""
)
```

@[3](Default param!)

+++

### Function

```kotlin
fun doSomething(
  foo: String, 
  bar: String = ""
) {
  //...
}
```

@[3](Default param!)

+++

## Extension functions

+++

```kotlin
fun String?.isNotNullOrBlank() = this?.isNotBlank() ?: false

//...

"PWR".isNotNullOrBlank()
```

+++

```kotlin
fun <T : Endpoint> List<T>.orderByLastUpdate() = this.sortedByDescending {
    it.jobStatuses.sortedByDescending { it.timestamp }.firstOrNull()?.timestamp ?: Timestamp.from(Instant.ofEpochMilli(1))
}
```

+++

## Inline functions

Note:
Mostly useful for Android devs I mostly use it along with...

+++

## Reified generics

Note:
Talk about type erasure
https://www.tutorialspoint.com/java_generics/java_generics_type_erasure.htm
https://stackoverflow.com/questions/12515861/how-to-check-if-a-generic-type-implements-a-specific-type-of-generic-interface-i

+++

```kotlin
inline fun <reified P : PipelineData> safeGetOtherData(): P {
    return (otherData.whenNotNull {
        if (it !is P) {
            throw PipelineException(
                this,
                "Other Data is not ${P::class.simpleName}"
            )
        }
        it
    } ?: throw PipelineException(
        this,
        "Other Data is null"
    )) as P
}
```

+++

## Type aliases

+++

```kotlin
typealias MoveGroupName = String
```

+++

## Named arguments

+++

```kotlin
data class User(
    var username: String,
    var password: String
)
```

+++

```kotlin
val user = User(
  password = "foo",
  username = "bar"
)
```

@[2-3](Named params)
@[2-3](The same can be applied to functions params)

Note:
Can use the same thing for functions

+++

## Infix functions

+++

```kotlin
infix fun String.like(other: String) = spec { p, _, _, b ->
    b.like(p.get(this), other)
}
```

@[1](`infix`)

+++


```kotlin
val callAgentSpec: Specification<Class5Rule> =
  where("bts" like it.sqlWildcards())
```

@[2](`like` - infix function)

+++

## Destructive declarations

+++

```kotlin
fun <T : Any, U : Deserializable<T>> Request.response(deserializable: U)
: Triple<Request, Response, Result<T, FuelError>> = 
try { //... }
```

@[2](Triplet)

+++

```kotlin
val (request, response, result) = "${URL}"
            .httpGet()
            .responseObject()
```

@[1](Desctructive declaration)

+++

```kotlin
val (_, _, result) = "${URL}"
            .httpGet()
            .responseObject()
```

@[1](We can ignore some of the values)

+++

## Operator overloading

+++

```kotlin
operator fun Sheet.get(n: Int): Row {
    return this.getRow(n) ?: this.createRow(n)
}

operator fun Row.get(n: Int): Cell {
    return this.getCell(n) ?: this.createCell(n)
}

operator fun Sheet.get(x: Int, y: Int): Cell {
    return this[x][y]
}

//...

sheet[0, 0]
```

@[1-3](Define get operator for Sheet, returning Row)
@[5-7](Define get operator for Row, returning Cell)
@[9-11](Define get operator for Sheet, returning Cell)

@[15](Access Sheet Cells in intuitive way!)

---

## Non-functional features

+++

## Easy

+++

## Code structure

Note:
The fact that you can have e.g. top level functions or multiple classes in single file, leads to clear project structure 

+++

## Standard library

+++

## Dynamic

* Coroutines
* Kotlin Native
* New versions bring in actual features
