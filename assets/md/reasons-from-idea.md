### Why JetBrains thinks it's good

* Tooling
* Learning Curve
* Interoperability
* Migration
* Scalability
* Expressiveness

Note:
Here are the reasons JetBrains gives as to why Kotlin is a good backend language
Let's take a look at evey one of them along with some examples

---

## Tooling
#### Well, it is IDEA after all

+++

### Easy conversion of Java code to Kotlin code

![Conversion](assets/image/convert-to-kotlin.gif)

Note:
It's not perfect though

+++

### Kotlin Plugin updates in sync with new IntelliJ releases

---

## Learning Curve

+++

### Trivial if you know Java
#### One week from zero to hero in our team

+++

### Easy for newcomers
#### Thanks to fantastic [documentation](https://kotlinlang.org/docs/reference/) and [koans](https://play.kotlinlang.org/koans/overview)

---

## Interoperability

+++

### Compiles to Java 6

Note:
> Compiles to Java 6
Will be missing some features though (coroutines)

+++

### Fully interoperable with Java

Note:
Add caveats here?

+++

### Migration can be gradual

Note:
Thanks to this interoperability, migration can be gradual

---

## Scalability

+++

### Coroutines

---

## Expressiveness

Note:
And finally expressiveness, but no use talking about that one - I'll show you some examples in the moment
