## But what is Kotlin?

Note:
Before we start, let me give you a quick overview of what Kotlin is

+++

### Statically typed programming language from JVM, Android and the browser - developed since 2010 by JetBrains

+++

### Basic syntax

```java
public static void main(String[] args) {
  User user = new User(
  	"JUG",
  	"hunter2"
  );

  final String username = user.getUsername();
}
```

@[1](Function declaration)
@[2](Variable initalization)
@[3-4](Set variable data)
@[7](Get data from variable)

+++

```kotlin
fun main(args: Array<String>) {
  var user = User(
      username = "JUG",
      password = "hunter2"
  )
  
  val username = user.username
}
```

@[1](Function declaration)
@[2](Variable initalization)
@[3-4](Set variable data)
@[7](Get data from variable)

