## But who uses it anyway?

Note:
So, now you see that Kotlin has some awesome features, are aware of the interoperability stuff - but does anybody actually supports and uses Kotlin? Sure!

+++

## Support:
* JetBrains (duh)
* Google (Android)
* Spring (from Spring 5)

+++

## Adoption

Backend:
* NetworkedAssets
* Ocado
* Prezi
* AWS
* Pinterest
* Coursera
* Netflix
* Uber
* Square
* Trello
* Basecamp
* Corda

+++

## Adoption

Android:
* Pinterest
* Expedia
* 3/4 of android companies with offers on Hackernews